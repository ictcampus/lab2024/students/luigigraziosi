package com.ictcampus.lab.base.repository.book.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Data
@Entity
@Table( name = "images" )
public class ImageEntity {
	@Id
	private Long id;

	private String title;
	private String url;

	@Column( name = "is_thumbnail" )
	private boolean thumbnail;

	@ManyToMany( mappedBy = "images" ) @ToString.Exclude
	List<BookEntity> books = new ArrayList<>();

	@OneToMany( mappedBy = "thumbnail", cascade = CascadeType.ALL, fetch = FetchType.LAZY ) @ToString.Exclude
	List<BookEntity> booksWithThumbnail = new ArrayList<>();

}
