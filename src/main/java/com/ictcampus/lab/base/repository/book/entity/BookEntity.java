package com.ictcampus.lab.base.repository.book.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@Data
@Table( name = "books" )
@Entity
public class BookEntity {
	@Id
	@GeneratedValue
	private Long id;
	private String title;
	private String isbn;

	@Column( name = "abstract" )
	private String descAbstract;
	private String description;
	private String publisher;

	@Column(name = "published_date")
	private LocalDate publishedDate;
	private BigDecimal price;
	private BigDecimal discount;

	@ManyToMany
	@JoinTable(
			name = "book_author",
			joinColumns = @JoinColumn(name = "book_id"),
			inverseJoinColumns = @JoinColumn(name = "author_id")
	) @ToString.Exclude
	private List<AuthorEntity> authors = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "thumbnail_id")
	private ImageEntity thumbnail;

	@ManyToMany
	@JoinTable(
			name = "book_image",
			joinColumns = @JoinColumn(name = "book_id"),
			inverseJoinColumns = @JoinColumn(name = "image_id")
	) @ToString.Exclude
	private List<ImageEntity> images = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "position_id")
	private PositionEntity position;

}