package com.ictcampus.lab.base.control.book.model;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@Value
@Builder
@Jacksonized
public class BookResponse {
	Long id;
	String title;
	String isbn;
	String descAbstract;
	String description;
	String publisher;
	LocalDate publishedDate;
	BigDecimal price;
	BigDecimal discount;
	List<AuthorResponse> authors;
}
