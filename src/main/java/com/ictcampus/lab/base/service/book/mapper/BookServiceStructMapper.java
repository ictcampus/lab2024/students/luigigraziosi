package com.ictcampus.lab.base.service.book.mapper;

import com.ictcampus.lab.base.repository.book.entity.BookEntity;
import com.ictcampus.lab.base.service.book.model.Book;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Mapper
public interface BookServiceStructMapper {
	List<Book> toBooks( final List<BookEntity> bookEntities );

	Book toBook( final BookEntity bookEntity );
}
