package com.ictcampus.lab.base.service.book.model;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Data
public class Book {
	private Long id;
	private String title;
	private String isbn;
	private String descAbstract;
	private String description;
	private String publisher;
	private LocalDate publishedDate;
	private BigDecimal price;
	private BigDecimal discount;
	private List<Author> authors;
	private Image thumbnail;
	private List<Image> images;
	private Position position;
}